package lime;


import lime.utils.Assets;


class AssetData {

	private static var initialized:Bool = false;
	
	public static var library = new #if haxe3 Map <String, #else Hash <#end LibraryType> ();
	public static var path = new #if haxe3 Map <String, #else Hash <#end String> ();
	public static var type = new #if haxe3 Map <String, #else Hash <#end AssetType> ();	
	
	public static function initialize():Void {
		
		if (!initialized) {
			
			path.set ("assets/data/data-goes-here.txt", "assets/data/data-goes-here.txt");
			type.set ("assets/data/data-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/intro.oel", "assets/data/intro.oel");
			type.set ("assets/data/intro.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Intro.oep", "assets/data/Intro.oep");
			type.set ("assets/data/Intro.oep", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/level1.oel", "assets/data/level1.oel");
			type.set ("assets/data/level1.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/pixelhole.ttf", "assets/data/pixelhole.ttf");
			type.set ("assets/data/pixelhole.ttf", Reflect.field (AssetType, "font".toUpperCase ()));
			path.set ("assets/data/playground.oel", "assets/data/playground.oel");
			type.set ("assets/data/playground.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Space.oep", "assets/data/Space.oep");
			type.set ("assets/data/Space.oep", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/animatedSpikes.png", "assets/images/animatedSpikes.png");
			type.set ("assets/images/animatedSpikes.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/bullet.png", "assets/images/bullet.png");
			type.set ("assets/images/bullet.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/falling1.png", "assets/images/falling1.png");
			type.set ("assets/images/falling1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/falling2.png", "assets/images/falling2.png");
			type.set ("assets/images/falling2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/gas.png", "assets/images/gas.png");
			type.set ("assets/images/gas.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/images-go-here.txt", "assets/images/images-go-here.txt");
			type.set ("assets/images/images-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/intro1.png", "assets/images/intro1.png");
			type.set ("assets/images/intro1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/intro2.png", "assets/images/intro2.png");
			type.set ("assets/images/intro2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/light.png", "assets/images/light.png");
			type.set ("assets/images/light.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/metalParticle.png", "assets/images/metalParticle.png");
			type.set ("assets/images/metalParticle.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/newtiles.png", "assets/images/newtiles.png");
			type.set ("assets/images/newtiles.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/newtiles2.png", "assets/images/newtiles2.png");
			type.set ("assets/images/newtiles2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/player.png", "assets/images/player.png");
			type.set ("assets/images/player.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/roomba.png", "assets/images/roomba.png");
			type.set ("assets/images/roomba.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/rover.png", "assets/images/rover.png");
			type.set ("assets/images/rover.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/ship.png", "assets/images/ship.png");
			type.set ("assets/images/ship.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/signs.png", "assets/images/signs.png");
			type.set ("assets/images/signs.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/slug.png", "assets/images/slug.png");
			type.set ("assets/images/slug.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/slugParticle.png", "assets/images/slugParticle.png");
			type.set ("assets/images/slugParticle.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/snail.png", "assets/images/snail.png");
			type.set ("assets/images/snail.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/spike1.png", "assets/images/spike1.png");
			type.set ("assets/images/spike1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/spike2.png", "assets/images/spike2.png");
			type.set ("assets/images/spike2.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/splash.png", "assets/images/splash.png");
			type.set ("assets/images/splash.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Title.png", "assets/images/Title.png");
			type.set ("assets/images/Title.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/turret.png", "assets/images/turret.png");
			type.set ("assets/images/turret.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/music/can.wav", "assets/music/can.wav");
			type.set ("assets/music/can.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/dead.wav", "assets/music/dead.wav");
			type.set ("assets/music/dead.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/gun.wav", "assets/music/gun.wav");
			type.set ("assets/music/gun.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/hit.wav", "assets/music/hit.wav");
			type.set ("assets/music/hit.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/intro.wav", "assets/music/intro.wav");
			type.set ("assets/music/intro.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/music-goes-here.txt", "assets/music/music-goes-here.txt");
			type.set ("assets/music/music-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/music/music.wav", "assets/music/music.wav");
			type.set ("assets/music/music.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/music2.wav", "assets/music/music2.wav");
			type.set ("assets/music/music2.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/pHit.wav", "assets/music/pHit.wav");
			type.set ("assets/music/pHit.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/roomba.wav", "assets/music/roomba.wav");
			type.set ("assets/music/roomba.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/rumble.wav", "assets/music/rumble.wav");
			type.set ("assets/music/rumble.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/snail.wav", "assets/music/snail.wav");
			type.set ("assets/music/snail.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/squish.wav", "assets/music/squish.wav");
			type.set ("assets/music/squish.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/step.wav", "assets/music/step.wav");
			type.set ("assets/music/step.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/sounds-go-here.txt", "assets/sounds/sounds-go-here.txt");
			type.set ("assets/sounds/sounds-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/beep.ogg", "assets/sounds/beep.ogg");
			type.set ("assets/sounds/beep.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/flixel.ogg", "assets/sounds/flixel.ogg");
			type.set ("assets/sounds/flixel.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			
			
			initialized = true;
			
		} //!initialized
		
	} //initialize
	
	
} //AssetData
