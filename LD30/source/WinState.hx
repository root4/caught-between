package ;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSubState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class WinState extends FlxSubState
{
	private var text:FlxText;
	private var button:FlxButton;

	public function new(BGColor:Int=FlxColor.TRANSPARENT) 
	{
		super(BGColor);
		FlxG.cameras.reset(new FlxCamera(0, 0, FlxG.width, FlxG.height));
		FlxG.camera.angle = 0;
		text = new FlxText(50, 0, 100, "Congratulations!!\n\nYou grabbed enough\nfuel to power up your speeder\nand leave these doomed planets behind.");
		text.alignment = "center";
		button = new FlxButton(60, 120, "Main Menu", function() {
			FlxG.switchState(new MenuState());
		});
		add(button);
		add(text);
	}
	
}