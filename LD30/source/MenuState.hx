package;

import flixel.addons.display.FlxStarField.FlxStarField2D;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.text.FlxBitmapFont;
import flixel.FlxBasic;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxBitmapTextField;
import flixel.text.FlxText;
import flixel.text.pxText.PxBitmapFont;
import flixel.text.pxText.PxTextAlign;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;


/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	private var background:FlxSprite;
	private var under:FlxSprite;
	private var stars:FlxStarField2D;
	private var stop:Bool;
	private var button:FlxButton;
	private var text:FlxText;

	override public function create():Void
	{
		super.create();
		FlxG.sound.playMusic("assets/music/music2.wav", 1);
		stars = new FlxStarField2D(0, 0, FlxG.width, FlxG.height);
		stars.scrollFactor.set(0, 0);
		background = new FlxSprite(0, 0);
		background.loadGraphic("assets/images/intro1.png", false, 256, 112);
		under = new FlxSprite(0,200);
		under.loadGraphic("assets/images/intro2.png", false, 256, 112);
		add(stars);
		add(background);
		add(under);
		FlxG.camera.focusOn(new FlxPoint(background.getMidpoint().x,background.getMidpoint().y +100));
		button = new FlxButton(0, 0, "Play",changeState);
		button.setPosition((FlxG.width / 2) - (button.width/2), 75);
		add(button);
		
		
		text = new FlxText(25, 100, 200, "Our laser-eyed cyclops finds himself \nstranded between two colliding worlds.\nHe needs to refuel his ship and leave before its too late.");
		text.alignment = "center";
	
		add(text);
		stop = false;
	}
	
	function changeState() 
	{
		FlxG.camera.fade(FlxColor.BLACK,1,false,function(){
		FlxG.switchState(new PlayState());
		});
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		if (!stop)
		{
		under.y --;
		background.y ++;
		}
		FlxG.overlap(under, background, stopM);
		
		super.update();
	}	
	
	function stopM(o1:FlxObject,o2:FlxObject) 
	{
		stop = true;
		under.y++;
		background.y --;
		FlxG.camera.shake(0.005, 2);
	}
}