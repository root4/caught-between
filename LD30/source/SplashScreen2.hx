package ;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.FlxSound;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class SplashScreen2 extends FlxState
{

		private var splash:FlxSprite;
		private var sound:FlxSound;
		override public function create():Void {
		super.create();
		
		splash = new FlxSprite(0, 0);
		splash.loadGraphic("assets/images/Title.png", false, 640, 480);
		splash.scale.set(.3, .3);
		sound = FlxG.sound.load("assets/music/intro.wav", 1, false);
		
		FlxG.camera.focusOn(splash.getMidpoint());
		
		add(splash);
		sound.play();
		FlxG.camera.fade(FlxColor.BLACK, 1, true, function() {
			FlxG.camera.fade(FlxColor.BLACK,3, false, function() {
				FlxG.switchState(new MenuState());
			});
		});
		
	
	}
}
	
