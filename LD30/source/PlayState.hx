package;

import enemies.Enemy;
import enemies.Roomba;
import enemies.Rover;
import enemies.Slug;
import enemies.Snail;
import environment.FallingBlock;
import environment.MovingSpikes;
import environment.Spike;
import environment.Turret;
import flixel.addons.api.FlxGameJolt;
import flixel.addons.display.FlxStarField.FlxStarField2D;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.addons.weapon.FlxWeapon;
import flixel.effects.particles.FlxEmitter;
import flixel.effects.particles.FlxParticle;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;
import flixel.util.FlxTimer;
import openfl.display.BlendMode;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	private var loader:FlxOgmoLoader;
	
	public var collisionLayerGrass:FlxTilemap;

	public var collisionLayerMetal:FlxTilemap;
	
	private var signLayer:FlxTilemap;
	
	private var player:Player;
	
	
	private var switchingPoint:Float;
	
	private var fallingBlocks:FlxGroup;
	private var spikes:FlxGroup;
	private var enemies:FlxGroup;
	private var traps:FlxGroup;
	private var gas:FlxGroup;
	private var turrets:Array<Turret>;
	
	private var slugParticle:FlxEmitter;
	private var metalParticle:FlxEmitter;
	private var deadParticle:FlxEmitter;

	
	private var buffer:FlxTimer;
	private var winTimer:FlxTimer;
	
	private var healthText:FlxText;
	
	private var stars:FlxStarField2D;
	
	private var shakeTimer:FlxTimer;
	
	private var fuel:Int;
	
	private var ship:FlxSprite;
	
	private var squish:FlxSound;
	private var roverHit:FlxSound;
	private var canCollect:FlxSound;
	private var rumble:FlxSound;
	private var roomba:FlxSound;
	private var pHit:FlxSound;
	private var snailHit:FlxSound;
	private var dead:FlxSound;
	
	
	
	
	override public function create():Void
	{
		
		FlxG.sound.volume = 1 ;
		FlxG.sound.playMusic("assets/music/music2.wav", 1, true);
		squish = FlxG.sound.load("assets/music/squish.wav", 1, false);
		roverHit = FlxG.sound.load("assets/music/hit.wav", .05, false);
		canCollect = FlxG.sound.load("assets/music/can.wav", .5, false);
		rumble = FlxG.sound.load("assets/music/rumble.wav", .5, false);
		roomba = FlxG.sound.load("assets/music/roomba.wav", 1, false);
		pHit = FlxG.sound.load("assets/music/pHit.wav", 1, false);
		snailHit = FlxG.sound.load("assets/music/snail.wav", 1, false);
		dead = FlxG.sound.load("assets/music/dead.wav", 1, false);
		fuel = 0;
		fallingBlocks = new FlxGroup();
		spikes = new FlxGroup();
		enemies = new FlxGroup();
		turrets = new Array<Turret>();
		traps = new FlxGroup();
		gas = new FlxGroup();
		
		buffer = new FlxTimer();
		
		healthText = new FlxText(0, 0,106, "lakjfd");
		healthText.scrollFactor.set(0, 0);
		
		shakeTimer = new FlxTimer(4, shake);
		winTimer = new FlxTimer();
		
		loader = new FlxOgmoLoader("assets/data/level1.oel");
		collisionLayerGrass = loader.loadTilemap("assets/images/newtiles.png", 16, 16, "collision1");
		collisionLayerMetal = loader.loadTilemap("assets/images/newtiles2.png", 16, 16, "collision2");
		signLayer = loader.loadTilemap("assets/images/signs.png", 16, 16, "signs");
		stars = new FlxStarField2D(0, 0, Std.int(collisionLayerGrass.width), Std.int(collisionLayerGrass.height));
		
		
		player = new Player(0, 0);
		player.health = 100;
		
		slugParticle = new FlxEmitter(0, 0);
		slugParticle.acceleration.y = 200;
		slugParticle.bounce = .3;
		for (i in 0...100)
		{
			var particle = new FlxParticle();
			particle.loadGraphic("assets/images/slugParticle.png", false, 1, 1);
			particle.allowCollisions = FlxObject.ANY;
			
			slugParticle.add(particle);
		}
	
		metalParticle = new FlxEmitter(0, 0);
		metalParticle.acceleration.y = -50;
		//metalParticle.bounce = .3;
		for (i in 0...100) {
			var mParticle = new FlxParticle();
			mParticle.loadGraphic("assets/images/metalParticle.png", false, 1, 1);
			mParticle.allowCollisions = FlxObject.ANY;
			
			metalParticle.add(mParticle);
		}
		
		deadParticle = new FlxEmitter(0, 0);
		for (i in 0...100)
		{
			var dParticle = new FlxParticle();
			dParticle.makeGraphic(1, 1, FlxColor.RED);
			dParticle.allowCollisions = FlxObject.ANY;
			
			deadParticle.add(dParticle);
		}
		
	
	
		
		loader.loadEntities(loadShit, "entities");
		
		
		player.setSwitch(switchingPoint);
		
		
		add(stars);
		add(signLayer);
		add(collisionLayerMetal);
		add(collisionLayerGrass);
		
		add(spikes);
		add(enemies);
		add(player);
		add(fallingBlocks);
		add(player.getGun().group);
		add(slugParticle);
		add(metalParticle);
		for ( i in 0...turrets.length)
		{
			add(turrets[i]);
			add(turrets[i].getGun().group);
		}
		add(traps);
		add(deadParticle);
		
		add(gas);
		add(ship);
		
		
		add(healthText);
		
		FlxG.camera.follow(player, FlxCamera.STYLE_LOCKON);
		
	
		this.set_bgColor(FlxColor.BLACK);
		
		//FlxG.debugger.drawDebug = true;
		
		
		super.create();
	}
	
	function shake(timer:FlxTimer) 
	{
		rumble.play();
		FlxG.camera.shake(0.009, .5);
		shakeTimer.start(FlxRandom.floatRanged(3,10), shake);
	}
	
	function loadShit(entityName:String, entityData:Xml) 
	{
		var x:Int = Std.parseInt(entityData.get("x"));
		var y:Int = Std.parseInt(entityData.get("y"));
		if (entityName == "player")
		{
			player.setPosition(x, y);
		}
		if (entityName == "switch")
		{
			this.switchingPoint = y;
		}
		if (entityName == "falling") {
			
			fallingBlocks.add(new FallingBlock(x, y, Std.parseInt(entityData.get("type"))));
		}
		if (entityName == "spike")
		{
			
			spikes.add(new Spike(x, y, Std.parseInt(entityData.get("type"))));
		}
		if (entityName == "enemy")
		{
			var type = Std.parseInt(entityData.get("type"));
			switch(type)
			{
				case 1: enemies.add(new Slug(x , y, this)); 
				case 2: enemies.add(new Snail(x, y, this)); 
				case 3: enemies.add(new Roomba(x, y, this)); 
				case 4: enemies.add(new Rover(x, y, this)); 
			}
		}
		if (entityName == "turret")
		{
			
			turrets.push(new Turret(x, y, entityData.get("facingRight")));
		}
		if (entityName == "movingSpikes")
		{
			traps.add(new MovingSpikes(x, y));
		}
		if (entityName == "gas")
		{
			var can = new FlxSprite(x, y);
			can.loadGraphic("assets/images/gas.png", true, 16, 16);
			can.animation.add("play", [0, 1, 2, 3], 8);
			can.animation.play("play");
			if (can.y < switchingPoint)
				can.flipY = true;
			gas.add(can);
			
			
		}
		if (entityName == "ship")
		{
			ship = new FlxSprite(x, y);
			ship.loadGraphic("assets/images/ship.png", true, 32, 16);
			ship.animation.add("idle", [0], 1);
			ship.animation.add("fly", [1, 2], 8);
			ship.animation.play("idle");
		}
	}

	override public function update():Void
	{
		
	
		healthText.text = "Health: " + player.health + " Fuel: " + fuel + "/5";
		
		if (player.flipped)
		{
		//	healthText.flipY = true;
		//	healthText.setFacingFlip(FlxObject.DOWN, true, true);
		//healthText.facing = FlxObject.DOWN;
		//	healthText.flipX = true;
			healthText.setPosition(0, 130);
			healthText.angle = 180;
		}
		else 
		{
			//healthText.setFacingFlip(FlxObject.UP, false, false);
			//healthText.facing = FlxObject.UP;
			healthText.setPosition(0, 0);
			healthText.angle = 0;
			//healthText.flipY = false;
			//healthText.flipX = false;
		}
		if (player.health <= 0 && player.alive)
			killPlayer();
		
		FlxG.collide(player, collisionLayerGrass);
		FlxG.collide(player, collisionLayerMetal);
		FlxG.collide(player, fallingBlocks, startFalling);
		
		FlxG.overlap(player, spikes, impale);
		FlxG.collide(player, enemies, hitEnemy);
		
		FlxG.collide(slugParticle, collisionLayerGrass);
		
		FlxG.overlap(player, traps,activateSpikes);
		
		FlxG.collide(metalParticle, collisionLayerMetal);
		for (i in 0...turrets.length)
		{
			turrets[i].getGun().bulletsOverlap(player, shootPlayer);
			turrets[i].getGun().bulletsOverlap(collisionLayerMetal, removeBullet);
			
		}
		
		player.getGun().bulletsOverlap(enemies, shootEnemy);
		player.getGun().bulletsOverlap(collisionLayerGrass, removeBullet);
		player.getGun().bulletsOverlap(collisionLayerMetal, removeBullet);
		
		FlxG.collide(deadParticle, collisionLayerGrass);
		FlxG.collide(deadParticle, collisionLayerMetal);
		
		FlxG.overlap(player, ship, win);
		
		
		//if (!FlxG.worldBounds.containsFlxPoint(player.getMidpoint()) && player.alive)
			//killPlayer();
		
			FlxG.overlap(player, gas, collectCan);
		
		super.update();
	}	
	
	function win(thisPlayer:Player,thisShip:FlxSprite) 
	{
		player.kill();
		ship.animation.play("fly");
		ship.velocity.x = 50;
		ship.velocity.y = -10;
		winTimer.start(3, winGame);
	}
	
	function winGame(timer:FlxTimer) 
	{
		this.openSubState(new WinState(FlxColor.BLACK));
	}
	
	function removeBullet(thisGround:FlxObject,thisBullet:FlxObject) 
	{
		thisBullet.kill();
	}
	
	function collectCan(thisPlayer:Player, thisCan:FlxSprite) 
	{
		thisCan.kill();
		fuel ++;
		canCollect.play();
		
	}
	
	function activateSpikes(thisPlayer:Player,thisSpike:MovingSpikes) 
	{
		thisSpike.startCycle();
		if (thisSpike.cycling)
		{
			if (FlxG.pixelPerfectOverlap(thisPlayer, thisSpike, 255))
				killPlayer();
		}
	}
	
	function shootPlayer(thisPlayer:FlxObject,thisBullet:FlxObject) 
	{
		
		killPlayer();
		thisBullet.kill();
		
	}
	
	function shootEnemy(thisEnemy:FlxObject, thisBullet:FlxObject) 
	{
		if (Std.is(thisEnemy, Rover))
			{
				thisEnemy.kill();
				metalParticle.setPosition(thisEnemy.x, thisEnemy.y);
				metalParticle.start(true, 1, 0.1, 13, 3);
				thisBullet.kill();
				roverHit.play();
			}
		
		else if (Std.is(thisEnemy, Snail))
			{
				snailHit.play();
				thisEnemy.kill();
				
				slugParticle.setPosition(thisEnemy.x, thisEnemy.y);
				slugParticle.start(true, 1, 0.1, 13, 3);
				thisBullet.kill();
			}
		
	}
	
	function hitEnemy(thisPlayer:Player, thisEnemy:Enemy) 
	{
		if (Std.is(thisEnemy, Snail))
			{
				pHit.play();
			player.health --;
			}
		if (Std.is(thisEnemy, Rover))
			{
				pHit.play();
				player.health --;
			}
		
		
		if (Std.is(thisEnemy, Roomba))
			{
				
	
				if ((player.y  > thisEnemy.y + 5) && player.velocity.y < 0)
				{
					metalParticle.setPosition(thisEnemy.x, thisEnemy.y);
				metalParticle.start(true, 1, 1, 13,3);
					thisEnemy.kill();
					roomba.play();
				}
				else
				{
					pHit.play();
					player.health --; 
				}
			}
		
		else if (Std.is(thisEnemy, Slug))
			{
				
				if ((player.y +5 < thisEnemy.y - thisEnemy.height) && player.velocity.y > 0)
					{thisEnemy.kill();
					slugParticle.setPosition(thisEnemy.x, thisEnemy.y);
				slugParticle.start(true, 1, 0.1, 13, 3);
				squish.play();
					}
				else
				{
					pHit.play();
					player.health --;
				}
			}
		
		
	}
	
	function impale(thisPlayer:Player,thisSpike:Spike) 
	{
		killPlayer();
		
	}
	function killPlayer()
	{
		dead.play();
		player.alive = false;
		deadParticle.setPosition(player.x, player.y);
		
		if (player.flipped)
			deadParticle.acceleration.y = -200;
		else
			deadParticle.acceleration.y = 200;
			
		deadParticle.start(true, 1, 0.1, 30, 3);
			
		player.kill();
		buffer.start(3, restart);
	}
	
	function restart(timer:FlxTimer) 
	{
		
		this.openSubState(new GameOver(FlxColor.RED));
	}
	
	function startFalling(thisPlayer:Player, thisBlock:FallingBlock) 
	{
		thisBlock.start();
		
	}
	
		override public function destroy():Void
	{
		super.destroy();
	}
}