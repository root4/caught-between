package enemies;
import flixel.FlxObject;

/**
 * ...
 * @author ...
 */
class Rover extends MetalEnemy
{

	public function new(x:Int, y:Int, state:PlayState) 
	{
		super(x, y, state);
		this.loadGraphic("assets/images/rover.png", true, 16, 16);
		this.animation.add("walk", [0, 1], 10);
		this.animation.play("walk");
		this.setFacingFlip(FlxObject.LEFT, false, true);
		this.setFacingFlip(FlxObject.RIGHT, true, true);
		this.flipY = true;
	}
	override public function destroy():Void 
	{
		this.attachment.destroy();
		super.destroy();
	}
	
	
}