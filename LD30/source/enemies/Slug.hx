package enemies;
import flixel.FlxObject;

/**
 * ...
 * @author ...
 */
class Slug extends Enemy
{
	
	public function new(x:Int, y:Int, state:PlayState) 
	{
		super(x +32, y, state);
		
		this.loadGraphic("assets/images/slug.png", true, 16, 8);
		this.animation.add("play", [0, 1], 4);
		this.setFacingFlip(FlxObject.RIGHT, true, false);
		this.setFacingFlip(FlxObject.LEFT, false, false);
		this.animation.play("play");
		this.acceleration.y = 3;
		
	}
	
}