package enemies;
import flixel.FlxObject;

/**
 * ...
 * @author ...
 */
class Snail extends Enemy
{

	public function new(x:Int, y:Int, state:PlayState) 
	{
		super(x, y, state);
		this.loadGraphic("assets/images/snail.png", true, 16, 16);
		this.animation.add("play", [0, 1,2,3,4], 4);
		this.setFacingFlip(FlxObject.RIGHT, true, false);
		this.setFacingFlip(FlxObject.LEFT, false, false);
		this.animation.play("play");
		
	}
	
}