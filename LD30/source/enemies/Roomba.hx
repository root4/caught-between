package enemies;

/**
 * ...
 * @author ...
 */
class Roomba extends MetalEnemy
{

	public function new(x:Int, y:Int, state:PlayState) 
	{
		super(x, y, state);
		this.loadGraphic("assets/images/roomba.png", true, 16, 8);
		this.animation.add("play", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 19);
		this.animation.add("dead", [13], 1);
		this.flipY = true;
		this.animation.play("play");
		//this.mass = .9;
		this.acceleration.y = -400;
		//this.drag.set(10, 10);
		
		
	}
	override public function update():Void {
		this.acceleration.x = 0;
		this.acceleration.y = 0;
		super.update();
	}
	
}