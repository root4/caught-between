package enemies ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class Enemy extends FlxSprite
{

	private var attachment:FlxSprite;
	private var state:PlayState;
	public function new(x:Int, y:Int,state:PlayState) 
	{
		super(x, y);
		//this.makeGraphic(16, 16, FlxColor.BLUE);
		attachment = new FlxSprite(x + 17, y+9);
		attachment.makeGraphic(8, 8, 0x00000000);
		this.velocity.x = 20;
		this.facing  = FlxObject.RIGHT;
		attachment.acceleration.y = .5;
		
		state.add(attachment);
		
		this.state = state;
		this.elasticity = 0;
		
	}
	
	override public function update():Void {
		
		FlxG.collide(attachment, state.collisionLayerGrass);
		FlxG.collide(attachment, state.collisionLayerMetal);
		FlxG.collide(this, state.collisionLayerGrass);
		FlxG.collide(this, state.collisionLayerMetal);
		
		
	if (this.velocity.x == 0)
		if (this.facing == FlxObject.RIGHT)
			{
				this.facing = FlxObject.LEFT;
				this.velocity.x = -20;
			}
			else {
				this.facing = FlxObject.RIGHT;
				this.velocity.x = 20;
			}
			
		if (!attachment.isTouching(FlxObject.ANY) )
		{
			if (this.facing == FlxObject.RIGHT)
			{
				this.facing = FlxObject.LEFT;
				this.velocity.x = -20;
			}
			else {
				this.facing = FlxObject.RIGHT;
				this.velocity.x = 20;
			}
			
		}
		
			if (this.facing == FlxObject.RIGHT)
			attachment.x = this.x + 18;
		else
			attachment.x = this.x - 8;
			
		super.update();
	}
	override public function kill():Void {
		this.attachment.kill();
		super.kill();
	}
}