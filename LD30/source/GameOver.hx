package ;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSubState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class GameOver extends FlxSubState
{

	private var text:FlxText;
	private var button:FlxButton;
	public function new(BGColor:Int=FlxColor.TRANSPARENT) 
	{
		super(BGColor);
		text = new FlxText(50, FlxG.height / 2, 100, "You Died....");
		text.alignment = "center";
		button = new FlxButton(60, 120, "Restart?", reset);
		
		FlxG.cameras.reset(new FlxCamera(0, 0, FlxG.width, FlxG.height));
		FlxG.camera.angle = 0;
		add(button);
		add(text);
		
		
	}
	override public function update():Void {
		FlxG.camera.angle = 0;
		super.update();
	}
	
	function reset() 
	{
		FlxG.resetState();
	}
	
}