package ;
import flixel.addons.weapon.FlxWeapon;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import flixel.util.FlxColor;
import flixel.util.FlxPoint;

/**
 * ...
 * @author ...
 */
class Player extends FlxSprite
{
	private var switchingPoint:Float;
	public var flipped:Bool;
	private var gun:FlxWeapon;
	private var step:FlxSound;
	private var stepTimer:Float;
	public function new(x:Int,y:Int) 
	{
		super(x, y);
		this.loadGraphic("assets/images/player.png", true, 16, 17);
		
		this.width = 9;
		this.offset.x = 3;
		this.animation.add("run", [0, 1, 2, 3, 4, 5, 6], 20);
		this.animation.add("falling", [8,9], 12);
		this.animation.add("jumping", [7], 1);
		this.animation.add("idle", [0], 1);
		this.setFacingFlip(FlxObject.RIGHT, false, false);
		this.setFacingFlip(FlxObject.LEFT, true, false);
		
		this.acceleration.y = 600;
		this.drag.set(1600, 100);
		this.maxVelocity.set(100, 1600);
		flipped = false;
		
		gun = new FlxWeapon("eyes", this);
		gun.makeImageBullet(50, "assets/images/bullet.png");
		gun.setParent(this, 0, 0, true);
		gun.setBulletSpeed(200);
		gun.bulletLifeSpan = .7;
		gun.setFireRate(500);
		gun.setBulletBounds(FlxG.worldBounds);
		gun.onFireSound = FlxG.sound.load("assets/music/gun.wav", .05);
		this.elasticity = 0;
	
		
		step = FlxG.sound.load("assets/music/step.wav", .2);
		stepTimer = .25;
	}
	public function getGun():FlxWeapon {
		return gun;
	}
	
	override public function update():Void {
		
		stepTimer -= FlxG.elapsed;
		if (flipped)
		{
			
		 if(FlxG.camera.angle != 180)
			FlxG.camera.angle+= 10;
		}
		else if (!flipped)
			if (FlxG.camera.angle != 0)
				FlxG.camera.angle -=10;
		this.acceleration.x = 0;
		if (this.velocity.x == 0)
		{
			
			this.animation.play("idle");
		
			
		}
		else if (this.isTouching(FlxObject.FLOOR) || this.isTouching(FlxObject.CEILING))
			{
				if (stepTimer <= 0)
				{
					stepTimer = .25;
					step.play(true);
				}
			}
		
		if (FlxG.keys.anyPressed(["A", "LEFT"]))
		{
			this.acceleration.x = -400;
		
		
			this.facing = FlxObject.LEFT;
			this.animation.play("run");
				if (flipped)
				{
					this.facing = FlxObject.RIGHT;
				this.acceleration.x = 400;
				}
			
		}
		else if (FlxG.keys.anyPressed(["D", "RIGHT"]))
		{
			
			this.acceleration.x = 400;
			
			this.facing = FlxObject.RIGHT;
			this.animation.play("run");
			if (flipped)
			{
				this.facing = FlxObject.LEFT;
				this.acceleration.x = -400;
			}
			
		}
		if (FlxG.keys.anyPressed(["W", "UP"]))
		{
			if (!flipped && this.isTouching(FlxObject.FLOOR))
				this.velocity.y = -220;
			else if(flipped && this.isTouching(FlxObject.CEILING))
				this.velocity.y = 220;
		}
		if (this.velocity.y < 0 && !this.isTouching(FlxObject.FLOOR))
		{
			this.animation.play("jumping");
		}
		if (this.velocity.y > 0 && !this.isTouching(FlxObject.FLOOR))
		{
			this.animation.play("falling");
		}
		
		if (this.y < switchingPoint && !flipped)
		{
			this.flipped = true;
			this.flipY = true;
			this.setFacingFlip(FlxObject.RIGHT, false, true);
		this.setFacingFlip(FlxObject.LEFT, true, true);
			this.acceleration.y = -600;
			
		}
		if (this.y > switchingPoint && flipped)
		{
			
			this.flipped = false;
			this.flipY = false;
				this.setFacingFlip(FlxObject.RIGHT, false, false);
		this.setFacingFlip(FlxObject.LEFT, true, false);
		this.acceleration.y = 600;
		}
		
		
		if (FlxG.keys.anyPressed(["SPACE"]))
		{
			if (flipped)
			{
				 
				gun.setBulletOffset(6, 11);
			}
			else
			{
				gun.setBulletOffset(6, 5);
			}
			gun.fire();
		}
		
		
		super.update();
	}
	
	public function setSwitch(switchingPoint:Float) 
	{
		this.switchingPoint = switchingPoint;
	}
	
	
	
}