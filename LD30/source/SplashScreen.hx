package ;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.FlxSound;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class SplashScreen extends FlxState
{

	private var splash:FlxSprite;
	private var sound:FlxSound;
	override public function create():Void {
		super.create();
		
		splash = new FlxSprite(0, 0);
		splash.loadGraphic("assets/images/splash.png", false,200, 150);
		sound = FlxG.sound.load("assets/music/intro.wav", 1, false);
		
		add(splash);
		sound.play();
		FlxG.camera.fade(FlxColor.BLACK, 1, true, function() {
			FlxG.camera.fade(FlxColor.BLACK,3, false, function() {
				FlxG.switchState(new SplashScreen2());
			});
		});
		
	}
}