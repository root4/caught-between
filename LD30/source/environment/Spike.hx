package environment ;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Spike extends FlxSprite
{

	public function new(x:Int,y:Int, type:Int ) 
	{
		super(x, y);
		
		if (type == 1)
		{
			this.loadGraphic("assets/images/spike1.png", false, 16, 16);
		}
		else {
			this.loadGraphic("assets/images/spike2.png", true, 16, 16);
			this.animation.add("play", [0, 1], 12);
			this.animation.play("play");
			this.flipY = true;
		}
	}
	
}