package environment;
import flixel.addons.effects.FlxWaveSprite.WaveMode;
import flixel.addons.weapon.FlxWeapon;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxRandom;
import flixel.util.FlxRect;

/**
 * ...
 * @author ...
 */
class Turret extends FlxSprite
{
	private var fireTimer:Float;
	private var gun:FlxWeapon;
	private var shooting:Bool;
	private var rando:FlxRandom;
	public function new(x:Int, y:Int,type:String) 
	{
		super(x, y);
		this.loadGraphic("assets/images/turret.png", true, 16, 16);
		this.animation.add("shoot", [0, 1, 2, 3, 4, 5, 6], 6);
		this.animation.play("shoot");
		
		this.flipY = true;
		gun = new FlxWeapon("turret");
		gun.makePixelBullet(50, 3, 1, FlxColor.RED);
		gun.setParent(this, 0, 8, true);
		gun.setBulletSpeed(200);
		gun.setBulletBounds(FlxG.worldBounds);
		this.setFacingFlip(FlxObject.LEFT, false, true);
		this.setFacingFlip(FlxObject.RIGHT, true, true);
		
		fireTimer = FlxRandom.floatRanged(0, 3);
		shooting = false;
		if(type == "False")
			this.facing = FlxObject.RIGHT;
		else
			this.facing = FlxObject.LEFT;
		
	}
	override public function update():Void {
		
		if (!shooting)
			fireTimer -= FlxG.elapsed;
		else {
			
			if (this.animation.curAnim.curIndex == 4)
			{
				fireTimer = .25;
				shooting = false;
				gun.fire();
			}
		}
		
		if (fireTimer <= 0)
			shooting = true;
		
		super.update();
	}
	public function getGun():FlxWeapon {
		return gun;
	}
	
}