package environment;
import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class MovingSpikes extends FlxSprite
{

	private var startTimer:Float;
	private var start:Bool;
	public var cycling:Bool;
	public function new(x:Int,y:Int) 
	{
		super(x, y);
		this.loadGraphic("assets/images/animatedSpikes.png", true, 16, 16);
		this.animation.add("play", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16], 7, false);
		startTimer = .25;
		
		start = false;
		cycling = false;
	}
	public function startCycle():Void {
		this.start = true;
	}
	override public function update():Void {
		
		if (start && startTimer >0)
			startTimer -= FlxG.elapsed;
		else if (start && startTimer <= 0)
		{
			animation.play("play");
			cycling = true;
			start = false;
		}
		if (cycling)
		{
			
			if (animation.finished)
			{
				startTimer = .25;
				cycling = false;
				start = false;
			}
		}
		
		super.update();
	}
	
}