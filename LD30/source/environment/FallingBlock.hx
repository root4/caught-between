package environment ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class FallingBlock extends FlxSprite
{

	private var timer:Float;
	private var falling:Bool;
	private var accel:Float;
	public function new(x:Int, y:Int, type:Int) 
	{
		super(x, y);
		timer = .25;
		falling = false;
		if (type == 1)
		{
			this.loadGraphic("assets/images/falling1.png", false, 16, 16);
			accel = 10;
			
		}
		else if (type ==2) {
			this.loadGraphic("assets/images/falling2.png", false, 16, 16);
			accel = -10;
		}
		
		this.immovable = true;
		this.maxVelocity.set(0.0000000000000, 50);
		
		
		
	}
	
	override public function update():Void {
		
		this.velocity.x = 0;
		this.acceleration.x = 0;
		
		if (falling) {
			if (timer <= 0)
			{
			this.immovable = false;
			this.acceleration.y = accel; 
			this.allowCollisions = FlxObject.ANY;
			}
			else
				timer -= FlxG.elapsed;
		}
		
		
		
		super.update();
	}
	public function start() {
		falling = true;
	}
	
}